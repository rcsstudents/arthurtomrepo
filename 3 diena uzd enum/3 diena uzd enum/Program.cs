﻿using System;

namespace _3_diena_uzd_enum
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.Lietotājs ievada datumu
            Console.WriteLine("Ievadi dienu: ");
            string diena = Console.ReadLine();
            int dienasNumurs = Convert.ToInt32(diena);

            Console.WriteLine("Ievadi mēnesi: ");
            string menesis = Console.ReadLine();
            int menesisNumurs = Convert.ToInt32(menesis);

            Console.WriteLine("Ievadi gadu: ");
            string gads = Console.ReadLine();
            int gadsNumurs = Convert.ToInt32(gads);


            //2. izveido datuma objektu
            DateTime datums = new DateTime(gadsNumurs, menesisNumurs, dienasNumurs);

            //3. Nolasīt DayOfWeek no datuma
            DayOfWeek dayOfWeek = datums.DayOfWeek;

            //4.  

            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    Console.WriteLine("Šī ir pirmdiena");
                    break;

                case DayOfWeek.Tuesday:
                    Console.WriteLine("Šī ir otrdiena");
                    break;

                case DayOfWeek.Wednesday:
                    Console.WriteLine("Šī ir trešdiena");
                    break;

                case DayOfWeek.Thursday:
                    Console.WriteLine("Šī ir ceturtdiena");
                    break;

                case DayOfWeek.Friday:
                    Console.WriteLine("Šī ir piektdiena");
                    break;

                case DayOfWeek.Saturday:
                    Console.WriteLine("Šī ir sestdiena");
                    break;

                case DayOfWeek.Sunday:
                    Console.WriteLine("Šī ir svētdiena");
                    break;
             

             default:
                Console.WriteLine("Diena netika atrasta");
                    break;
            }

            if(dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
                Console.WriteLine("Tā ir brīvdiena");
            else
            {
                Console.WriteLine("Tā ir darba diena");
            }


            Console.ReadKey();

        }
    }
}
