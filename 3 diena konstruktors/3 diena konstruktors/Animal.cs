﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_diena_konstruktors
{
    public class Animal : Food, IWalking
    {
        public void DoWalking()
        {
            Console.WriteLine("Dzīvnieks nogāja 20 km");
        }
    }
}
