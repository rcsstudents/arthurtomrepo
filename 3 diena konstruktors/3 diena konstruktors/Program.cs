﻿using System;
using System.Collections.Generic;

namespace _3_diena_konstruktors
{
    class Program
    {
        static void Main(string[] args)


            Animal animal = new Animal();
            Person person = new Person();

            animal.Eat();
            animal.Drink();
            
            person.Eat();
            person

        public void Diena3(){
            List<IWalking> walkingList = new List<IWalking>();
            List<Person> personList = new List<Person>();
            List<Programmer> programmerList = new List<Programmer>();

            string turpinat = "";
            while (turpinat != "n")
            {
                Console.WriteLine("Ievadi vārdu: ");
                string vards = Console.ReadLine();

                Console.WriteLine("Ievadi augumu: ");
                string augums = Console.ReadLine();
                double augumsDouble = Convert.ToDouble(augums);

                Console.WriteLine("Ievadi svaru: ");
                string svars = Console.ReadLine();
                double svarsDouble = Convert.ToDouble(svars);

                Console.WriteLine("Ievadi matu krāsu: ");
                string krasa = Console.ReadLine();

                Console.WriteLine("Ievadi mīļāko dienu (no 1-7): ");
                string dayOfWeek = Console.ReadLine();
                int dayOfWeekInt = Convert.ToInt32(dayOfWeek);
                DayOfWeek diena = (DayOfWeek)dayOfWeekInt;


                Person cilveks = new Person(vards, augumsDouble, svarsDouble, krasa, diena);
                personList.Add(cilveks);

                Programmer programmer = new Programmer(vards, augumsDouble, svarsDouble, krasa, "C#");
                programmer.WritePersonInfo();

                Animal animal = new Animal();
                animal.Eat();


                walkingList.Add(cilveks);
                walkingList.Add(animal);

                Console.WriteLine("Turpināt? y/n");
                turpinat = Console.ReadLine();
            }

            foreach (Person person in personList)
            {
                person.WritePersonInfo();
            }

            foreach (Programmer programmer in programmerList)
            {
                programmer.WritePersonInfo();
            }

            foreach (IWalking walking in walkingList)
            {
                walking.DoWalking();
            }

            Console.ReadKey();
        }
    }
}

