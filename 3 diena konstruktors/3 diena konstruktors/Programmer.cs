﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_diena_konstruktors
{
    class Programmer : Person
    {
        public string ProgrammingLanguage;

        public Programmer (string name, double height, double weight, string hairColor, string programmingLanguage):
            base (name, height, weight, hairColor)
        {
            ProgrammingLanguage = programmingLanguage;
        }

        public override void WritePersonInfo()
        {
            Console.WriteLine($"Proogrammētāja {Name} augums ir {Height} centimetri, bet svars - {Weight} kilogrami.");
            Console.WriteLine($"matukrāsa - {HairColor}");
            Console.WriteLine($"mīļākā programmēšanas valoda ir - C#");
      
        }

    }
}
