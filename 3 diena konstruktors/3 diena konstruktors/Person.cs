﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_diena_konstruktors
{
    
    class Person : IWalking
    {
        public string Name;
        public double Height;
        public double Weight;
        public string HairColor;
        private DayOfWeek? FavoriteWeekDay;

        public Person(string name, double height, double weight, string hairColor, DayOfWeek? favoriteWeekDay = null)
        {
            Name = name;
            Height = height;
            Weight = weight;
            HairColor = hairColor;
            FavoriteWeekDay = favoriteWeekDay;
        }

        public virtual void WritePersonInfo()
        {
            Console.WriteLine($"Personas {Name} augums ir {Height} centimetri, bet svars - {Weight} kilogrami.");
            Console.WriteLine($"matukrāsa - {HairColor}");
            Console.WriteLine($"mīļākā diena - {FavoriteWeekDay}");
        }

        //public void walk()
        //{
        //    console.writeline("persona nogāja 10 km");
        //}


        public void DoWalking()
        {
            Console.WriteLine("Persona nogāja 10 km");
        }

        public override void Drink()
        {

        }
    }
}
