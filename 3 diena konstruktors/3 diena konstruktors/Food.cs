﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3_diena_konstruktors
{
    public abstract class Food
    {
        //Nesatur funkcionalitāti
        //obligāti vajag override šo metodi
        //public abstract void DrinkAbstract();

        //Satur funkcionalitāti
        //var override šo metodi, ja nepieciešams
        public virtual void Drink()
        {
            Console.WriteLine("I drink beer!");
        }

        //satur funkcionalitāti;
        //nevar override
        public override void Eat()
        {

        }

        public void Eat()
        {
            Console.WriteLine("I like food");
        }
    }
}
