﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4_diena_uzd_1
{
    class Student : Person, IPerson
    {
        public Student (string name, string surname)
        {
            Name = name;
            Surname = surname;
        }


        public string GetHomework()
        {
            string Fullname = GetFullName();
            return $"Student {Fullname} has homework."; 
        }

        public override string ReturningText()
        {
            return "kaut kāds teksts";
        }
      
    }
}
