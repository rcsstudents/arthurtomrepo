﻿using System;
using System.Collections.Generic;

namespace _4_diena_uzd_1
{
    class Program
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("ievadiet vārdu?");
                string name = Console.ReadLine();

            Console.WriteLine("ievadiet uzvārdu?");
                string surname = Console.ReadLine();

            List<IPerson> persons = new List<IPerson>();
            persons.Add(new Student(name, surname));
            persons.Add(new Professor(name, surname));


            foreach (IPerson person in persons)
            {
                Console.WriteLine(person.GetHomework());
            }


            Console.ReadKey();
            
        }
   
    }

}
