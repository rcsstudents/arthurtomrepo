﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _4_diena_uzd_1
{
    class Professor : Person, IPerson
    {
        public Professor (string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public string GetHomework()
        {
            string Fullname = GetFullName();
            return $"Professor {Fullname} has no homework.";
        }
    }
}
