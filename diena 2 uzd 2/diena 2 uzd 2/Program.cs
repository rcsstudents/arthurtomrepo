﻿using System;

namespace diena_2_uzd_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Lūdzu ievadiet (jebkādu) vārdu" );
            string vards = Console.ReadLine();

            int burti = vards.Length;
            Console.WriteLine("Vārda garums ir: " + burti);

            int patskani = 0;
            foreach (char c in vards)
                
            {
                if ('a' == c || 'e' == c || 'i' == c || 'u' == c || 'o' == c) 
                {
                    patskani++;
                }    
            }

            Console.WriteLine("Vārdā ir " + patskani + " patskaņi");
            Console.ReadKey();
        }
    }
}
