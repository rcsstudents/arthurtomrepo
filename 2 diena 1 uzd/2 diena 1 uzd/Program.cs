﻿using System;
using System.Collections.Generic;

namespace _2_diena_1_uzd
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyDataTypes> myDataTypesList = new List<MyDataTypes>();

            Console.WriteLine("Izpildīt programmu? (y/n)");
            while (Console.ReadLine() != "n")

            {

                MyDataTypes myDataTypes = new MyDataTypes();    
            //1.Bool apstrāde
            Console.WriteLine("Ievadiet bool vērtību (true/false)");
            string myBoolString = Console.ReadLine();
                myDataTypes.MyBool = Convert.ToBoolean(myBoolString);

            //2. Byte apstrāde
            Console.WriteLine("Ievadiet byte vērtību");
            string myByte = Console.ReadLine();

            bool isConverted = byte.TryParse(myByte, out byte result);
            if (isConverted == true)
            {
                    myDataTypes.MyByte = Convert.ToByte(myByte);
            }

            //3. Short apstrāde
            Console.WriteLine("Ievadiet short vērtību");
            string myShort = Console.ReadLine();
                myDataTypes.MyShort = Convert.ToInt16(myShort);

            //4. Int apstrāde
            Console.WriteLine("Ievadiet Int vērtību");
            string myInt = Console.ReadLine();
                myDataTypes.MyInt = Convert.ToInt32(myInt);

            //5. Long apstrāde
            Console.WriteLine("Ievadiet Long vērtību");
            string myLong = Console.ReadLine();
                myDataTypes.MyLong = Convert.ToInt64(myLong);

            //6. Double apstrāde
            Console.WriteLine("Ievadiet Double vērtību");
            string myDouble = Console.ReadLine();
                myDataTypes.MyDouble = Convert.ToDouble(myDouble);

            //7. Decimal apstrāde
            Console.WriteLine("Ievadiet Decimal vērtību");
            string myDecimal = Console.ReadLine();
                myDataTypes.MyDecimal = Convert.ToDecimal(myDecimal);

            //8. String apstrāde
            Console.WriteLine("Ievadiet String vērtību");
            string myString = Console.ReadLine();
                myDataTypes.MyString = Convert.ToString(myString);

            //9. Char apstrāde
            Console.WriteLine("Ievadiet Char vērtību");
            string myChar = Console.ReadLine();
                myDataTypes.MyChar = Convert.ToChar(myChar);
            }

            foreach (MyDataTypes myData in myDataTypesList)
            {
                Console.WriteLine("MyBool = " + myData.MyBool);
                Console.WriteLine("MyByte = " + myData.MyByte);
                Console.WriteLine("MyShort = " + myData.MyShort);
                Console.WriteLine("MyInt = " + myData.MyInt);
                Console.WriteLine("MyLong = " + myData.MyLong);
                Console.WriteLine("MyDouble = " + myData.MyDouble);
                Console.WriteLine("MyDecimal = " + myData.MyDecimal);
                Console.WriteLine("MyString = " + myData.MyString);
                Console.WriteLine("MyChar = " + myData.MyChar);
            }

            Console.ReadKey();
            
        }
    }
}
